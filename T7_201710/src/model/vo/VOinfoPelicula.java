package model.vo;

public class VOinfoPelicula 
{
	//Atributos 

	private String titulo ;
	private  int anio;
	private  String director;
	private  String actores;
	private double ratingIMDB;
	private String movieId;

    public void setMovieId(String pMovieId)
    {
    	movieId = pMovieId;
    }
    public String  getMovieId()
    {
    	return movieId;
    }
	public void setTitulo( String pTitulo)
	{
		titulo = pTitulo;
	}
	public String getTitulo()
	{
		return titulo;
	}
	public void setAnio(int pAnio)
	{
		anio = pAnio;
	}
	public int getAnio()
	{
		return anio;
	}
	public void setDirector (String pDirector)
	{
		director = pDirector;
	}
	public String getDirector()
	{
		return director;
	}
	public void setActores( String pActores)
	{
		actores = pActores;
	}
	public String getActores()
	{
		return actores;
	}
	public void setRating( double pRatingIMDB)
	{
		ratingIMDB = pRatingIMDB;
	}
	public double getRatingIMDB()
	{
		return ratingIMDB ;
	}

}
