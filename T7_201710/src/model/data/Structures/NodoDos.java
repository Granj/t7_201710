package model.data.Structures;

public class NodoDos<K extends Comparable<K>,V> implements Comparable<NodoDos<K,V>> {

	private K key;
	private V value;
	private NodoDos<K,V> menor;
	private NodoDos<K,V> mayor;
	private boolean red;
	private static boolean RED=true;
	
	
	public NodoDos(K kKey, V vValue) {
		// TODO Auto-generated constructor stub
		key = kKey;
		value = vValue;
		menor=null;
		mayor=null;
		red=RED;
	}
	
	public void cambiarMenor(NodoDos<K,V> pMenor){
		menor=pMenor;
	}
	public void cambiarMayor(NodoDos<K,V> pMayor){
		mayor=pMayor;
	}
	
	public K darKey(){
		return key;
		
	}
	public void cambiarValor(V pValue){
		value=pValue;
	}
	public V darValue(){
		return value;
	}
	public NodoDos<K, V> darMayor(){
		return mayor;
	}
	public NodoDos<K, V> darMenor(){
		return mayor;
	}
	public void cambiarColor(){
		if(red)red=false;
		else{
			red=true;
		}
	}
	public boolean isRed(){
		return red;
	}
	public NodoDos<K,V> rotarDer(NodoDos<K,V> n){
		n.cambiarMayor(this);
		n.cambiarMenor(this.menor);
		return n;
		
	}
	public NodoDos<K,V> rotarIzq(NodoDos<K,V> n){
		n.cambiarMenor(this);
		n.cambiarMayor(this.mayor);
		return n;
	}
	public void flipColor(){
		if(menor.red){
			menor.cambiarColor();
		}
		if(mayor.red){
			mayor.cambiarColor();
		}
	}
	public void agregar(NodoDos<K,V> n){
		//evalua los nodos que pueden tener dos enlaces rojos seguidos.
		boolean b = menor.isRed()&&menor.menor.isRed();
		boolean d= menor.isRed()&&menor.mayor.isRed();
		boolean e = mayor.isRed()&&mayor.mayor.isRed();
		
		if(n.equals(this)){
			this.cambiarValor(n.darValue());
		}
		else if(menor==null&&n.compareTo(this)<0){
			menor=n;
		}
		else if(mayor==null&&n.compareTo(this)>0){
			mayor=n;
			mayor.cambiarColor();
		}
		else{
			if(n.compareTo(this)<0){
				menor.agregar(n);
				if(b){
					menor = menor.rotarDer(n);
					menor.flipColor();
				}
				if(d){
					menor.mayor.cambiarColor();
				}
			}
			if(n.compareTo(mayor)>0){
				mayor.agregar(n);
				if(e){
					mayor=mayor.rotarIzq(n);
					mayor.flipColor();
				}
			}
		}
	}
	
	public NodoDos buscarNodo(NodoDos buscado){
		if(buscado.equals(this)){
			return this;
		}
		else if(buscado.compareTo(this)>0){
			return mayor.buscarNodo(buscado);
		}
		else if (buscado.compareTo(this)<0){
			return menor.buscarNodo(buscado);
		}
		else{
			return null;
		}
		
	}
	public NodoDos[] eliminarNodo(NodoDos aEliminar){
		if(this.buscarNodo(aEliminar)==null){
			return null;
		}
		else{
			if(aEliminar.equals(this)){
				NodoDos[] hijos = new NodoDos[3];
				hijos[1]= mayor;
				hijos[0]=menor;
				hijos[3]=this;
				return hijos;
				
			}
			else if(aEliminar.compareTo(this)>0){
				NodoDos[] hijos = mayor.eliminarNodo(aEliminar);
				mayor.cambiarMayor(hijos[1]);
				mayor.cambiarMenor(hijos[0]);
				if(mayor.menor.isRed()==false){
					mayor.menor.cambiarColor();
				}
				if(mayor.mayor.isRed()){
					mayor.mayor.cambiarColor();
				}
				return hijos;
				
			}
			else if(aEliminar.compareTo(this)<0){
				NodoDos[] hijos = menor.eliminarNodo(aEliminar);
				menor.cambiarMayor(hijos[1]);
				menor.cambiarMenor(hijos[0]);
				if(mayor.menor.isRed()==false){
					mayor.menor.cambiarColor();
				}
				if(mayor.mayor.isRed()){
					mayor.mayor.cambiarColor();
				}
				return hijos;
				
			}
		}
		return null;
	}
	
	public int compareTo(NodoDos<K, V> o) {
		// TODO Auto-generated method stub
		return key.compareTo(o.key);
	}
	

}
