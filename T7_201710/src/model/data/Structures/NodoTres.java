package model.data.Structures;

public class NodoTres<K extends Comparable<K>,V>  implements Comparable<NodoTres<K,V>> {
	private NodoDos<K, V> menor;
	private NodoDos<K, V> mayor;
	
	

	public NodoTres(NodoDos<K,V> pMenor,NodoDos<K,V> pMayor) throws Exception
	{
		// TODO Auto-generated constructor stub
		if(pMenor.compareTo(pMayor)>0){
			throw new Exception("no es la llave menor");
		}
		menor=pMenor;
		mayor=pMayor;
	}



	public int compareTo(NodoTres<K, V> o) {
		// TODO Auto-generated method stub
		return menor.compareTo(o.menor);
	}

}
