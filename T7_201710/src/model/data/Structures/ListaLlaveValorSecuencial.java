package model.data.Structures;


public class ListaLlaveValorSecuencial<K,V> 
{
	//Atributos 

	private Nodo primer;
	private int tamano;
	private Nodo ultimo;


	//Metodos

	/**
	 *Crea la lista vacía.
	 */
	public ListaLlaveValorSecuencial()
	{
		primer = null;
		tamano = 0;
		ultimo = null;
	}
	/**
	 * Devuelve el número de llaves en la lista.
	 * @return el numero de llaves en la lista.
	 */
	public int darTamanio()
	{
		return tamano;
	}
	/**
	 *  Devuelve si la lista está vacía o no
	 */
	public boolean estaVacia()
	{
		return tamano == 0;
	}
	/**
	 * Devuelve si hay un elemento en la lista con la llave
	 * @param llave
	 */
	public boolean tieneLlave(K llave)
	{
		Nodo actual = primer;
		while(actual != null)
		{
			if(( actual.darLlave().equals(llave)))
			{
				return true;
			}
			actual = actual.darSiguiente();
		}
		return false;
	}
	/**
	 * @param llave.
	 * @return el valor asociado a la llave, devuelve null si no existe la llave en la lista.
	 */

	public V darValor(K llave)
	{
		if(tieneLlave(llave))
			return null;
		else
		{
			V valRet = null;
			Nodo actual = primer;
			boolean encontro = false;
			while(actual != null && !encontro )
			{
				if(actual.darLlave().equals(llave))
				{
					valRet = (V) actual.darValor();
					encontro = true;
				}
				else
					actual = actual.darSiguiente();
			}
			return valRet;
		}
	}

	/**
	 * Inserta en la lista la llave y el valor.
	 * Si el valor es null y la llave existe debe eliminar el nodo de la lista.
	 * Si la llave no existe debe añadir a la lista un nuevo nodo.
	 * Si la llave existe debe reemplazar el valor del nodo.
	 */
	public void insertar(K llave, V valor)
	{
		//Caso en que la lista este vacia
		if (estaVacia())
		{
			Nodo add = new Nodo(llave, valor);
			primer = add;
			ultimo = add;
		}

		//Caso en que tiene la llave.
		else if(tieneLlave(llave))
		{
			//Tiene la llave y el valor es nulo.
			if(valor.equals(null))
			{
				//Elimina el primer
				if(primer.darLlave().equals(llave))
					primer = primer.darSiguiente();
				else
				{
					//Elimina un intermedio o el final 
					Nodo actual = primer;
					boolean elimino = false;
					while(actual != null && !elimino)
					{
						if(actual.darSiguiente().darLlave().equals(llave))
						{
							actual.eliminarSiguiente();
							elimino = true;
							if(actual.darSiguiente().equals(null))
								ultimo = actual;
						}
						actual = actual.darSiguiente();
					}
				}	
			}
			//Sobre escribe la llave.
			else 
			{
				Nodo actual = primer;
				boolean agrego = false;
				while(actual!= null & !agrego)
				{
					if(actual.darLlave().equals(llave))
						actual.cambiarValue(valor);
				}
			}
		}
		//Se agrega el elemento al final si la llave no estaba
		else
		{
			Nodo add = new Nodo(llave, valor);
			Nodo temp = ultimo ;
			ultimo.cambiarSiguiente(add);
			ultimo = add;
		}
	}
	//no esta dejando a�adirlo bien
	
}
