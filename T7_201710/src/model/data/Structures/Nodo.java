package model.data.Structures;



public class Nodo<K, V> 
{
	private	K key;
	private	V value;
	private	Nodo siguiente;

	public Nodo(K pKey, V pValue)
	{
		key = pKey;
		value = pValue;
		siguiente = null;
	}
	public Nodo darSiguiente()
	{
		return siguiente;
	}

	public V darValor()
	{
		return value;
	}

	public K darLlave()
	{
		return key;
	}

	public void cambiarSiguiente(Nodo Pagregar)
	{
		siguiente = Pagregar;
	}

	public void cambiarValue(V pValue)
	{
		value = pValue;
	}
	public void eliminarSiguiente()
	{
		this.cambiarSiguiente(siguiente.darSiguiente());
	}


}
