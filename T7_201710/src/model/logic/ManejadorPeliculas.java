
package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Date;

import model.data.Structures.ListaLlaveValorSecuencial;
import model.data.Structures.RBTree;
import model.vo.VOinfoPelicula;
import com.google.gson.Gson;
public class ManejadorPeliculas 
{
	RBTree arbolRb=new RBTree<Integer, RBTree<Integer,VOinfoPelicula>>();
	
	public void cargarJson(String pRuta)
	{
		Gson lectorGson= new Gson();
		try
		{
		BufferedReader lector= new BufferedReader(new FileReader(pRuta));
			VOinfoPelicula[] peliAdd= lectorGson.fromJson(lector, VOinfoPelicula[].class);
			for(VOinfoPelicula aux:peliAdd)
			{
				Integer anio= (int)aux.getAnio();
				Integer id= Integer.parseInt(aux.getMovieId());
				if(((Object) arbolRb.darRaiz().darValue()!=null))
				{
					((RBTree)arbolRb.get(anio)).put(id, aux);
				}
				else{
					arbolRb.put(anio, new RBTree<Integer, VOinfoPelicula>());
					((RBTree)arbolRb.get(anio)).put(id, aux);
				}
			}
		
		}
		catch(Exception e){
			System.out.println("no se pudo cargar el archivo: "+ e.getMessage());
		}
	}
	
	
	public ListaLlaveValorSecuencial<Date, RBTree> AgnoMasPeliculas( )
	{
		return arbolRb.inOrdenBack();
	}
	public void peliculasAno( Date fecha, Long agno )
	{
		RBTree<Long, VOinfoPelicula> peliculas = (RBTree<Long, VOinfoPelicula>) arbolRb.get(fecha);
		ListaLlaveValorSecuencial<Long, VOinfoPelicula> listaPeliculas = new ListaLlaveValorSecuencial<>();
		for( long i = 0; i < listaPeliculas.darTamanio(); i++ )
		{
			System.out.println(listaPeliculas.darValor(i));
		}
	}
}
	
